from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_photo(city, state):
    headers = {
        "Authorization": PEXELS_API_KEY,
    }

    params = {
        "per_page": 1,
        "query": city + " " + state,
    }

    url = "https://api.pexels.com/v1/search"
    res = requests.get(url, params=params, headers=headers)
    content = json.loads(res.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except:
        return {"picture_url": None}


def get_weather_data(city, state):
    params_geo = {
        "q": city + "," + state + "," + "US",
        "appid": OPEN_WEATHER_API_KEY,
    }
    url_geo = "http://api.openweathermap.org/geo/1.0/direct"
    res_geo = requests.get(url_geo, params=params_geo)
    content_geo = json.loads(res_geo.content)
    try:
        lat = content_geo[0].get("lat")
        lon = content_geo[0].get("lon")
    except IndexError:
        return None

    params_weather = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
    }
    url_weather = "https://api.openweathermap.org/data/2.5/weather"
    res_weather = requests.get(url_weather, params=params_weather)
    content_weather = json.loads(res_weather.content)
    try:
        return {
            "weather": {
                "temp": content_weather["main"]["temp"],
                "description": content_weather["weather"][0]["description"],
            }
        }

    except:
        return {
            "weather": None,
        }
